# Repositori DAA   
## Perbandingan *Genetic Algorithm* dan Pendekatan *Dynamic Programming* untuk 0/1 *Knapsack Problem*

Anggota:
- Hendrico Setiawan - 1906350912
- Jonathan Amadeus - 1906400261
- Muhammad Hanif Fahreza - 1906351026

Folder:

* TC  
Berisikan test cases yang digunakan untuk percobaan.
* Algoritma
Berisikan *dynamic programming* dan *genetic algorithm* untuk menyelesaikan masalah 0/1 *knapsack* dan file Main untuk menjalankan kedua algoritma.
* RESULT  
Berisikan output dari test cases yang digunakan.