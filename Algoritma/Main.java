import java.io.*;
import java.util.*;

public class Main {
    private static InputReader in;
    private static PrintWriter out;
    public static void main(String[] args) {
        InputStream inputStream = System.in;
        in = new InputReader(inputStream);
        OutputStream outputStream = System.out;
        out = new PrintWriter(outputStream);
        DPKnapsack dp = new DPKnapsack();
        GAKnapsack ga;
        ArrayList<Integer> val = new ArrayList<>();
        ArrayList<Integer> wt = new ArrayList<>();
        int n = in.nextInt();
        for (int i = 0; i < n; i++) {
            val.add(in.nextInt());

        }
        for (int i = 0; i < n; i++) {
            wt.add(in.nextInt());
        }
        int q = in.nextInt();
        long start, stop; 
        out.println("DPOut,DPTime,GAOut,GATime,GA Total Time,isSame,OutDiff,TimeDiffBest,TimeDiffTotal,N,Kp,N*Kp");
        //out.println("DP Out, DP Time, GA Out, GA Time,GA Total Time, Attempt, isFinish, Query");
        String s = "";
        for (int i = 0; i < q; i++) {
            s = "";
            int W = in.nextInt();
            start = System.nanoTime();
            int dpOut = dp.knapSack(W, wt, val, n);
			stop = System.nanoTime();
            s += dpOut + ",";
            long dpTime = stop - start;
            s += dpTime + ",";
            int attempt = 0;
            ga = new GAKnapsack(n, val, wt, W,n,100,0.8, 0.1 );
            start = System.nanoTime();
            ga.doIt();
            stop = System.nanoTime();
            double gaOut = ga.getBestResult();
            s += gaOut + ",";
            long gaTime = stop - start;
            s += ga.getBestTime() + ",";
            s += gaTime + ",";
            s += (dpOut == gaOut) + ",";
            s += (dpOut - gaOut) + ",";
            s += (dpTime - ga.getBestTime()) + ",";
            s += (dpTime - gaTime) + ",";
            s += n + ",";
            s += W + ",";
            s += n*W + "";
            s += "\n";
            out.print(s);
            out.flush();
        }
        
        out.close();
    }       

    static class InputReader {
        public BufferedReader reader;
        public StringTokenizer tokenizer;
 
        public InputReader(InputStream stream) {
            reader = new BufferedReader(new InputStreamReader(stream), 32768);
            tokenizer = null;
        }
 
        public String next() {
            while (tokenizer == null || !tokenizer.hasMoreTokens()) {
                try {
                    tokenizer = new StringTokenizer(reader.readLine());
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
            return tokenizer.nextToken();
        }
 
        public int nextInt() {
            return Integer.parseInt(next());
        }

        public long nextLong() {
            return Long.parseLong(next());
        }
 
    }
}
