import java.util.*;
import java.util.Random;
/***
author:@Akshay
 */

public class GAKnapsack{

    static Scanner sc;
    static Random rand = new Random();
    private int crossover_count = 0;
    private int clone_count = 0;
    private int no_items = 0;
    private int population_size = 0;
    private int maximum_generations = 0;
    private int best_result = -1;
    private int last_best_generation = 0;
    private int knapsack_capacity = 0;
    private double prob_crossover = 0;
    private double prob_mutation = 0;
    private int total_fitness_of_generation = 0;
    private long start_time;
    private long best_time;
    private ArrayList<Long> generation_time = new ArrayList<Long>();
    private ArrayList<Integer> value = new ArrayList<Integer>();
    private ArrayList<Integer> weight = new ArrayList<Integer>();
    private ArrayList<Integer> fitness = new ArrayList<Integer>();
    private ArrayList<Integer> fitness_index = new ArrayList<Integer>();
    private ArrayList<String> population = new ArrayList<String>();
    private ArrayList<String> next_gen = new ArrayList<String>();
    private HashMap<Integer, Integer> fitnessValPop = new HashMap<Integer, Integer>();

    public GAKnapsack(int noItems, ArrayList<Integer> values, ArrayList<Integer> weights, int knapsackSize, int populationSize,
            int maxGenerations, double crossProb, double mutatProb) {
        no_items = noItems;
        value = values;
        weight = weights;
        knapsack_capacity = knapsackSize;
        population_size = populationSize;
        maximum_generations = maxGenerations;
        prob_crossover = crossProb;
        prob_mutation = mutatProb;

        for (int i = 0; i < noItems; i++) {
            fitness_index.add(i);
        }
    }

    public long getBestTime() {
        return best_time;
    }

    public int getBestResult() {
        return best_result;
    }

    public int getLastBestGen() {
        return last_best_generation;
    }

    public void doIt() {
        //Time start
        start_time = System.nanoTime();

        //Generate population
        this.generatePopulation();

        int max_note = maximum_generations;
        boolean over90Percent = false;
        
        while(maximum_generations + 1 > 0 || ! over90Percent){
            //Check fitness
            int fittest_index = this.evaluateFitness();

            if (best_result < fitness.get(fittest_index)) {
                best_result = fitness.get(fittest_index); 
                last_best_generation = max_note - maximum_generations;
            }

            // printStuffs(max_note, fittest_index);

            if (! over90Percent) {
                over90Percent = check90Percent();
            }

            if (maximum_generations + 1 <= 0 && over90Percent) {
                break;
            }

            ArrayList<Integer> fitness_index_sorted = new ArrayList<>(fitness_index);
            Collections.sort(fitness_index_sorted, Comparator.comparing(index -> fitness.get(index)));
            Collections.reverse(fitness_index_sorted);
            fitness.clear();

            int elites = elitismOn(fitness_index_sorted, true);

            //Handle odd number of items
            if (population_size % 2 == 1) {
                // next_gen.add(generation_solution.get(generation_counter - 1));
                next_gen.add(population.get(fitness_index_sorted.get(0 + elites)));
            } 

            for(int i = 0; i < population_size - elites / 2; i++){

                int gene1=selectGene(fitness_index_sorted);
                int gene2=selectGene(fitness_index_sorted);

                crossoverGenes(gene1,gene2);
                mutateGene();
            }

            //Time end in a generation
            generation_time.add(System.nanoTime());

            for(int i=0;i<population_size;i++){
                // System.out.println("#"+(i+1)+" "+next_gen.get(i));
                population.remove(i);
                population.add(i,next_gen.get(i));
            }
            next_gen.clear();

            maximum_generations--;
        }

        Collections.sort(generation_time);
        best_time = generation_time.get(0) - start_time;
        // System.out.println(best_result);
    }

    private int selectGene(List<Integer> fitness_index_sorted) {
        int selected_index = -1;
        int random_selector = rand.nextInt(100);
        int items_avail = fitness_index_sorted.size();
        int random_index_selector = rand.nextInt((int) (items_avail)/4);

        if (random_selector < 50) {
            selected_index = random_index_selector;
        } else if (random_selector >= 50 && random_selector < 80) {
            selected_index = random_index_selector + (int) items_avail/4;
        } else if (random_selector >= 80 && random_selector < 95) {
            selected_index = random_index_selector + (int) items_avail/2;
        } else {
            selected_index = random_index_selector + (int) 3*items_avail/4;
        }

        return fitness_index_sorted.get(selected_index);
    }

    private void crossoverGenes(int gene_1, int gene_2) {
        String new_gene_1;
        String new_gene_2;

        double rand_crossover = Math.random();
        if(rand_crossover <= prob_crossover) {
            // Perform crossover
            crossover_count = crossover_count + 1;
            Random generator = new Random(); 
            int cross_point = generator.nextInt(no_items) + 1;

            new_gene_1 = population.get(gene_1).substring(0, cross_point) + population.get(gene_2).substring(cross_point);
            new_gene_2 = population.get(gene_2).substring(0, cross_point) + population.get(gene_1).substring(cross_point);

            next_gen.add(new_gene_1);
            next_gen.add(new_gene_2);
        }
        else {
            clone_count = clone_count + 1;
            next_gen.add(population.get(gene_1));
            next_gen.add(population.get(gene_2));
        }
    }

    private void mutateGene() {
        double rand_mutation = Math.random();
        if(rand_mutation <= prob_mutation) {

            String mut_gene;
            String new_mut_gene;
            Random generator = new Random();
            int mut_point = 0;
            double which_gene = Math.random() * 1;

	    // Mutate gene
            if(which_gene <= 0.5) {
                mut_gene = next_gen.get(next_gen.size() - 1);
                mut_point = generator.nextInt(no_items);
                if(mut_gene.substring(mut_point, mut_point + 1).equals("1")) {
                    new_mut_gene = mut_gene.substring(0, mut_point) + "0" + mut_gene.substring(mut_point+1);
                    next_gen.set(next_gen.size() - 1, new_mut_gene);
                }
                if(mut_gene.substring(mut_point, mut_point + 1).equals("0")) {
                    new_mut_gene = mut_gene.substring(0, mut_point) + "1" + mut_gene.substring(mut_point+1);
                    next_gen.set(next_gen.size() - 1, new_mut_gene);
                }
            }
            if(which_gene >0.5) {
                mut_gene = next_gen.get(next_gen.size() - 2);
                mut_point = generator.nextInt(no_items);
                if(mut_gene.substring(mut_point, mut_point + 1).equals("1")) {
                    new_mut_gene = mut_gene.substring(0, mut_point) + "0" + mut_gene.substring(mut_point+1);
                    next_gen.set(next_gen.size() - 1, new_mut_gene);
                }
                if(mut_gene.substring(mut_point, mut_point + 1).equals("0")) {
                    new_mut_gene = mut_gene.substring(0, mut_point) + "1" + mut_gene.substring(mut_point+1);
                    next_gen.set(next_gen.size() - 2, new_mut_gene);
                }
            }           
        }
    }


    private int evaluateFitness(){
        total_fitness_of_generation = 0;
        int fitest = 0;
        int fitest_index = 0;

        for(int j = 0; j < population_size; j++) {
            int total_weight = 0;
            int total_value = 0;
            char c = '1';

            for(int i = 0; i < no_items; i++) {
                c = population.get(j).charAt(i);
                //chromosome value 1
                if(c == '1') {
                    total_weight = total_weight + weight.get(i);
                    total_value = total_value + value.get(i);
                }
            }
            int diff = knapsack_capacity - total_weight;

            if (diff < 0) { //If the gene weights more than capacity, change it
                String old_gene = population.get(j);
                String new_gene = "";
                boolean gene_over_capacity = true;

                while (gene_over_capacity) {
                    int bit_to_change = rand.nextInt(no_items);
                    c = old_gene.charAt(bit_to_change);

                    if (c == '1') {
                        new_gene = old_gene.substring(0, bit_to_change) + '0' + old_gene.substring(bit_to_change+1);

                        if (total_weight - weight.get(bit_to_change) <= knapsack_capacity) {
                            population.set(j, new_gene);
                            total_value -= value.get(bit_to_change);
                            gene_over_capacity = false;
                        } else {
                            old_gene = new_gene;
                            total_weight -= weight.get(bit_to_change);
                            total_value -= value.get(bit_to_change);
                        }
                    }
                }
            }
            fitness.add(total_value);

            if (fitnessValPop.containsKey(total_value)) {
                fitnessValPop.put(total_value, fitnessValPop.get(total_value) + 1);
            } else {
                fitnessValPop.put(total_value, 1);
            }
            
            if(total_value > fitest){
                fitest = total_value;
                fitest_index = j;
            }
            total_fitness_of_generation = total_fitness_of_generation + total_value;
            
            }
            return fitest_index; 
    }

    private void generatePopulation(){
        // System.out.println("Population:");
        for(int i = 0; i < population_size; i++) {
            String gene = "";

            for(int j = 0; j < no_items; j++) {
                                
                if(Math.random() > 0.5) {
                    gene+="1";
                }
                else{
                    gene+="0";
                }
            }
            // System.out.println("#"+(i+1)+" "+gene);
            population.add(gene);      
        }
    }

    private void printStuffs(int max_note, int fittest_index) {
        System.out.println("Generation #" + (max_note - maximum_generations + 1));
            System.out.println("\nFitness:");
            for(int m = 0; m < this.population_size; m++) {
                System.out.println((m + 1) + " - " + this.fitness.get(m));
            } 

            System.out.println("\nTotal Generation Fitness "+total_fitness_of_generation);
            System.out.print("The fittest chromosome of this generation is "+ population.get(fittest_index));
            System.out.println(" And its fitness is "+ fitness.get(fittest_index));
            System.out.println("Crossover occurred " + this.crossover_count + " times");
            System.out.println("Cloning occurred " + this.clone_count + " times");
            if(clone_count==0) {
                System.out.println("Mutation did not occur\n");
            }
            else{
                System.out.println("Mutation did occur\n");
            }
    }

    private int elitismOn(ArrayList<Integer> fitness_index_sorted,boolean on) {
        if (on) {
            //Elitism
            next_gen.add(population.get(fitness_index_sorted.get(0)));
            next_gen.add(population.get(fitness_index_sorted.get(1)));
            return 2;
        }
        return 0;
    }

    private boolean check90Percent() {
        for (Map.Entry<Integer, Integer> set : fitnessValPop.entrySet()) {
            if (set.getValue()/population_size >= 9/10) {
                return true;
            }
        }
        return false;
    }
}